FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY bower.json ./
COPY package*.json ./
COPY Gruntfile.js ./

RUN npm install -g grunt grunt-cli bower
RUN npm install
RUN bower install --allow-root
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 9001
CMD [ "grunt", "serve" ]

